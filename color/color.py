from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib import colors
import matplotlib.pyplot as plt
import numpy as np
import cv2
from datetime import datetime
import sys


flags = [i for i in dir(cv2) if i.startswith('COLOR_')]

#my painting
image = sys.argv[1]
painting = cv2.imread(image)
painting = cv2.cvtColor(painting, cv2.COLOR_BGR2RGB)

# process
r, g, b = cv2.split(painting)
fig = plt.figure()
axis = fig.add_subplot(1, 1, 1, projection="3d")

pixel_colors = painting.reshape((np.shape(painting)[0]*np.shape(painting)[1], 3))
norm = colors.Normalize(vmin=-1.,vmax=1.)
norm.autoscale(pixel_colors)
pixel_colors = norm(pixel_colors).tolist()

# creating the axis
axis.scatter(r.flatten(), g.flatten(), b.flatten(), facecolors=pixel_colors, marker=".")
axis.set_xlabel("Red")
axis.set_ylabel("Green")
axis.set_zlabel("Blue")

#plt.show()

dateTimeObj = datetime.now()
timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")
plt.savefig(timestampStr+'.png')
