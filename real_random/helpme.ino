#include <SparkFun_ATECCX08a_Arduino_Library.h> //Click here to get the library: http://librarymanager/All#SparkFun_ATECCX08a
#include <Wire.h>

ATECCX08A atecc;

void setup() {
  // put your setup code here, to run once:
  Wire.begin();
  Serial.begin(115200);
  if (atecc.begin() == true)
  {
    Serial.println("Successful wakeUp(). I2C connections are good.");
  }
  else
  {
    Serial.println("Device not found. Check wiring.");
    while (1); // stall out forever
  }

  printInfo(); // see function below for library calls and data handling

  // check for configuration
  if (!(atecc.configLockStatus && atecc.dataOTPLockStatus && atecc.slot0LockStatus))
  {
    Serial.print("Device not configured. Please use the configuration sketch.");
    while (1); // stall out forever.
  }

}

void loop() {
  // put your main code here, to run repeatedly:
  long color_n = atecc.random(5);
  long value_n = atecc.random(2);

  color(color_n,value_n);
  delay(5000);

}

void color(int r_num, int r_val)
{

  int white = 0;
  int red = 1;
  int blue = 2;
  int yellow = 3;
  int green = 4;
  int black = 5;

  if ( r_num == white ){
    Serial.println("white");
  } else if ( r_num == red ){
    Serial.println("red");
  } else if ( r_num == blue ){
    Serial.println("blue");
  } else if ( r_num == yellow ){
    Serial.println("yellow");
  } else if ( r_num == green ){
    Serial.println("green");
  } else if ( r_num == black ){
    Serial.println("black");
  } else {
    Serial.println("color choice error");
  }

  if ( r_val == 1 ){
    Serial.println("soft");  
  } else {
    Serial.println("strong");
  }
  
}


void printInfo()
{
  // Read all 128 bytes of Configuration Zone
  // These will be stored in an array within the instance named: atecc.configZone[128]
  atecc.readConfigZone(false); // Debug argument false (OFF)

  // Print useful information from configuration zone data
  Serial.println();

  Serial.print("Serial Number: \t");
  for (int i = 0 ; i < 9 ; i++)
  {
    if ((atecc.serialNumber[i] >> 4) == 0) Serial.print("0"); // print preceeding high nibble if it's zero
    Serial.print(atecc.serialNumber[i], HEX);
  }
  Serial.println();

  Serial.print("Rev Number: \t");
  for (int i = 0 ; i < 4 ; i++)
  {
    if ((atecc.revisionNumber[i] >> 4) == 0) Serial.print("0"); // print preceeding high nibble if it's zero
    Serial.print(atecc.revisionNumber[i], HEX);
  }
  Serial.println();

  Serial.print("Config Zone: \t");
  if (atecc.configLockStatus) Serial.println("Locked");
  else Serial.println("NOT Locked");

  Serial.print("Data/OTP Zone: \t");
  if (atecc.dataOTPLockStatus) Serial.println("Locked");
  else Serial.println("NOT Locked");

  Serial.print("Data Slot 0: \t");
  if (atecc.slot0LockStatus) Serial.println("Locked");
  else Serial.println("NOT Locked");

  Serial.println();

  // omitted printing public key, to keep this example simple and focused on just random numbers.
}
